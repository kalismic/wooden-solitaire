# Dřevěný solitér

## O projektu

Jedná se o semestrální práci na předmet vývoj klientských aplikací v javascriptu. Cílem projektu bylo vytvořit fungující hru dřevěný solitér.

### Popis hry

Na začátku hry je herní plán plný dřevěných kolíčků, kromě střední pozice je, která je prázdná.\
Cílem hry je, aby na herním plánu zůstal pouze jeden kolíček.\
Tahy jsou obdobné tahům ve hře dáma, kolíčkem přeskakujeme kolíček sousední na prázdné pole zaním. Narozdíl od dámy nejsou tahy šikmé, ale vodorovné a svislé.

## Popis funkčnosti

Při vytváření hry jsem našel druhou variantu rozmístění dřevěných kolíčku na desce, proto při spouštění nové hry si hráč může vybrat variantu, kterou chce.\
Hráč nejprve vybere kliknutím kolíček, kterým bude hýbat. Kolíček se mu označí a označí se mu i všechny možné tahy. Druhýmm kliknutím hráč vybírá místo, kam se má kolíček přesunout.\
Při povedeném tahu přibyde kolíček v postraním panelu. Postraní panel má vždy počet děr stejný, jako je počet kolíčků, které je během hry nutno odstranit, aby hráč vyhrál.\
Pokud hráč vyhraje, zobrazí se mu tabulka o tom, že vyhrál, pokud hráč prohraje zobrazí se mu tabulka, o tom že prohrál. Hráč vždy může zvolit zda bude hrát hru další a nebo prozatím přestane.\
Při aktualizaci aplikace zůstane stav hry zachován.\
S prvním tahem se spouští hudba na pozadí hry, která se přestává přehrávat s koncem hry.\
V aplikaci je plánováno další rozšíření, které však není v rozsahu semestrální práce.

## Kategorie, které nebyly implementovány z důvodu nehodící se funkcionality

- Do aplikace nebyl zahrnut canvas ani SVG - nenašel jsem pro ně v aplikaci využití.
- V aplikaci nebyli využity ani vendor prefixy, protože nebyla využita, žádná složitější funkcionalita.