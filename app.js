/**
 * initial capacity of 1.version of game
 */
const objects1 = [
    { size: 3},
    { size: 3},
    { size: 7},
    { size: 7},
    { size: 7},
    { size: 3},
    { size: 3}
];

/**
 * initial capacity of 2.version of game
 */
const objects2 = [
    { size: 3},
    { size: 5},
    { size: 7},
    { size: 7},
    { size: 7},
    { size: 5},
    { size: 3}
];

/**
 * default setting of game version
 */
let objects = objects1

/**
 * represent pin and also gap
 */
class Pin {
    constructor(row, column) {
        this.row = row
        this.column = column
        this.element = null
    }

    /**
     * change gap to full pin
     */
    fill() {
        this.element.classList.remove('emptyPin')
        this.element.classList.add('fullPin')
    }

    /**
     * mark space around pin
     */
    mark() {
        this.element.classList.add('marked')
    }

    /**
     * Set html element
     * @param {HTMLElement} element 
     */
    setElement(element) {
        this.element = element
    }
}

/**
 * represent game plan
 */
class Plan {
    constructor(parent, dock) {
        this.parent = parent
        this.dock = dock
        this.array = []
        this.current = null
        this.gameIsOver = false
        this.music = null
        this.musicIsPlaying = false
    }

    /**
     * start music if not plaing
     */
    musicPlay() {
        if (this.music == null) {
            this.music = new Audio('Scanglobe - Badawi.mp3')
            this.music.play()
            this.musicIsPlaying = true
        } else if (!this.musicIsPlaying) {
            this.music.play()
            this.musicIsPlaying = true
        }
    }

    /**
     * stop music if playing
     */
    musicStop() {
        if (this.musicIsPlaying) {
            this.music.pause()
            this.musicIsPlaying = false
        }
    }

    /**
     * 1.step - choose pin which is want to play
     * 2.step - move pin to new position
     * @param {Pin} pin 
     * @param {number} row index of end position
     * @param {number} column index of end position
     */
    move(pin, row, column) {
        if (!this.gameIsOver) {
            if (this.current == null) {
                this.current = pin
                if (this.current.element.className != 'emptyPin') {
                    pin.mark()
                    this.findPossibleMoves()
                } else {
                    this.current = null
                }
            } else {
                if (this.isPossible(row, column)
                && this.array[row][column].element.classList.contains('emptyPin')
                && this.isPinBetween(row, column, this.current.row, this.current.column)) {
                    this.array[row][column].element.classList.remove('emptyPin')
                    this.array[row][column].element.classList.add('fullPin')
                    localStorage.setItem(row + '' + column, 'fullPin')
                    this.current.element.classList.remove('fullPin')
                    this.current.element.classList.add('emptyPin')
                    localStorage.setItem(this.current.row + '' + this.current.column, 'emptyPin')
                    this.dropSkipped(this.current.row, this.current.column, row, column)
                    this.musicPlay()
                } else if (row != this.current.row || column != this.current.column) {
                    alert("Error! This move is not possible.")
                }
                this.cancelMarked(this.current.row, this.current.column)
                this.current = null
            }
        }
    }
    
    /**
     * 
     * @param {number} row index of end position
     * @param {number} column index of end position
     * @param {number} currentRow index of start position
     * @param {number} currentColumn index of start position
     * @returns true if pin with class fullPin is between position [row, column] and [currentRow, currentColumn], else return false
     */
    isPinBetween(row, column, currentRow, currentColumn) {
        if (row != currentRow
        && this.array[(row + currentRow) /2][column].element.classList.contains('fullPin')) {
            return true
        } else if (column != currentColumn
        && this.array[row][(column + currentColumn) /2].element.classList.contains('fullPin')) {
            return true
        }
        return false
    }

    /**
     * set position between [startRow, startColumn] and [endRow, endColumn] to emptyPin
     * control if game is over
     * @param {number} startRow index of start position
     * @param {number} startColumn index of start position
     * @param {number} endRow index of end position
     * @param {number} endColumn index of end position
     */
    dropSkipped(startRow, startColumn, endRow, endColumn) {
        let row
        let column
        if (startRow != endRow) {
            row = (startRow + endRow) / 2
            column = startColumn
        } else if (startColumn != endColumn) {
            row = startRow
            column = (startColumn + endColumn) / 2
        }
        this.array[row][column].element.classList.remove('fullPin')
        this.array[row][column].element.classList.add('emptyPin')
        localStorage.setItem(row + '' + column, 'emptyPin')
        this.dock.add()
        if (this.controlGameEnd()) {
            this.musicStop()
            this.gameIsOver = true
            this.dock.showLooserBox()
            this.dock.toggleModalState()
            this.dock.addButtonListener()
        }
    }

    /**
     * 
     * @returns true if game is over else return false
     */
    controlGameEnd() {
        let result = false
        for (let row = 0; row < this.array.length; row++) {
            for (let column = 0; column < this.array.length; column++) {
                if (this.array[row][column].element.classList.contains('fullPin')) {
                    result = this.findPossibleMoves(row, column)
                    this.cancelMarked(row, column)
                    if (result) {
                        return false
                    }
                }
            }
        }
        return true
    }

    /**
     * unset mark from all mark position
     * @param {number} row index of start position of move 
     * @param {number} column index of start position of move
     */
    cancelMarked(row, column) {
        this.unmarkIfPossible(row, column)
        this.unmarkIfPossible(row + 2, column)
        this.unmarkIfPossible(row - 2, column)
        this.unmarkIfPossible(row, column + 2)
        this.unmarkIfPossible(row, column - 2)
    }

    /**
     * unset mark position if position is in game plan
     * @param {number} row index of position
     * @param {number} column index of position
     */
    unmarkIfPossible(row, column) {
        if (row < objects.length && row > -1 && column < objects.length && column > -1) {
            this.array[row][column].element.classList.remove('marked')
        }
    }

    /**
     * render game plan and set new game button listener
     */
    render() {
        for (let row = 0; row < objects.length; row++) {
            const amount = objects[row].size
            let pinSet = []
            const pinSetElement = document.createElement('div');
            pinSetElement.setAttribute('class', 'pinSet')
            for (let column = 0; column < objects.length; column++) {
                const pin = new Pin(row, column)
                pinSet.push(pin)
                const pinElement = document.createElement('div');
                if (column >= (objects.length - amount)/ 2 && column < (objects.length + amount)/ 2) {
                    pinElement.setAttribute('class', 'emptyPin')
                    pinElement.addEventListener('click', () => this.move(pin, row, column))
                } else {
                    pinElement.setAttribute('class', 'disabledPin')
                }
                pinSetElement.appendChild(pinElement)
                pin.setElement(pinElement)
            }
            this.array.push(pinSet)
            this.parent.appendChild(pinSetElement)
        }
        const newGameButton = document.querySelector('li.new-game')
        newGameButton.addEventListener('click', () => {
                this.dock.showNewGameBox()
                this.dock.toggleModalState()
                this.dock.addButtonListener()
        })
    }

    /**
     * 
     * @param {number} r index of position
     * @param {number} c index of position
     * @returns true if any move from position [r, c] is possible, else return false
     */
    findPossibleMoves(r = this.current.row, c = this.current.column) {
        let result = this.markIfPossible(r+2, c, r, c)
        let currentResult = this.markIfPossible(r-2, c, r, c)
        result = result ? result : currentResult
        currentResult = this.markIfPossible(r, c+2, r, c)
        result = result ? result : currentResult
        currentResult = this.markIfPossible(r, c-2, r, c)
        result = result ? result : currentResult
        return result
    }

    /**
     * 
     * @param {number} row index of end position
     * @param {number} column index of end position
     * @param {number} currentRow index of start position
     * @param {number} currentColumn index of start position
     * @returns false if is not any position marked, else return true
     */
    markIfPossible(row, column, currentRow, currentColumn) {
        let result = false
        if (row < objects.length && row > -1 && column < objects.length && column > -1) {
            if (row != currentRow) {
                if (this.array[row][column].element.classList.contains('emptyPin')
                && this.array[(row + currentRow) /2][column].element.classList.contains('fullPin')) {
                    this.array[row][column].element.classList.add('marked')
                    result = true
                }
            } else {
                if (this.array[row][column].element.classList.contains('emptyPin')
                && this.array[row][(column + currentColumn) /2].element.classList.contains('fullPin')) {
                    this.array[row][column].element.classList.add('marked')
                    result = true
                }
            }
        }
        return result
    }

    /**
     * 
     * @param {number} row index of end position
     * @param {number} column index of end position
     * @returns true if some move is possible, else return false
     */
    isPossible(row, column) {
        if (row + 2 == this.current.row && column == this.current.column) {
            return true
        } else if (row - 2 == this.current.row && column == this.current.column) {
            return true
        } else if (row == this.current.row && column + 2 == this.current.column) {
            return true
        } else if (row == this.current.row && column - 2 == this.current.column) {
            return true
        }
        return false
    }

    /**
     * save all important information to localStorage
     */
    saveAll() {
        for (let row = 0; row < this.array.length; row++) {
            for (let column = 0; column < this.array.length; column++) {
                localStorage.setItem(row + '' + column, this.array[row][column].element.className)
            }
        }
    }

    /**
     * load all available information from localStorage
     */
    loadSaved() {
        for (let row = 0; row < this.array.length; row++) {
            for (let column = 0; column < this.array.length; column++) {
                this.array[row][column].element.className = localStorage.getItem(row + '' + column)
            }
        }
    }

    /**
     * clear game plan
     */
    clear() {
        this.parent.remove()
        this.parent = document.createElement('div')
        this.parent.setAttribute('id', 'plan')
        const mainElement = document.querySelector('main')
        mainElement.appendChild(this.parent)
        this.array = []
        this.current = null
        this.gameIsOver = false
    }

    /**
     * fill game plan to initial condition if no information is saved
     * else load saved state
     */
    init() {
        if (localStorage['00'] == null) {
            for (let row = 0; row < objects.length; row++) {
                for (let column= 0; column < objects.length; column++) {
                    if ((row != (objects.length - 1) / 2 
                    || column != (objects.length - 1) / 2) 
                    &&  !this.array[row][column].element.classList.contains('disabledPin')) {
                        this.array[row][column].fill()
                    }
                }
            }
            this.array[(objects.length - 1) / 2][(objects.length - 1) / 2].element.classList.remove('fullPin')
            this.array[(objects.length - 1) / 2][(objects.length - 1) / 2].element.classList.add('emptyPin')
            this.saveAll()
        } else {
            this.loadSaved()
        }
    }
}

/**
 * panel to skipped pin
 */
class Dock {
    constructor(parent) {
        this.parent = parent
        this.modalVisible = false
        this.array = []
        this.index = 0
        this.sum = 0
        this.myPlan = null
    }

    /**
     * set game plan
     * @param {Plan} myPlan 
     */
    setPlan(myPlan) {
        this.myPlan = myPlan
    }

    /**
     * 
     * @returns sum of all pins you must skipped to win
     */
    sumOfPins() {
        const values = objects.map(x => x.size)
        this.sum = values.reduce((x, y) => {
            let value = x + y
            return value
        })
        return this.sum
    }

    /**
     * add skipped pin
     */
    add() {
        this.array[this.index].element.classList.remove('emptyPin')
        this.array[this.index].element.classList.add('fullPin')
        this.index += 1
        if (localStorage.getItem('dock') == null) {
            localStorage.setItem('dock', 1)
        } else {
            localStorage.setItem('dock', (localStorage.getItem('dock') * 1) + 1)
        }
        if (this.index == this.sum - 2) {
            this.myPlan.musicStop()
            this.myPlan.gameIsOver = true
            this.showWinnerBox()
            this.toggleModalState()
            this.addButtonListener()
        }
    }
    
    /**
     * start new game and reset game plan and dock
     */
    runNewGame() {
        this.myPlan.musicStop()
        this.gameIsOver = false
        localStorage.clear()
        this.myPlan.clear()
        this.myPlan.render()
        this.myPlan.init()
        this.clear()
        this.render()
    }

    /**
     * add listeners to all available buttons
     */
    addButtonListener() {
        const modalButtons = document.querySelectorAll('.modal-button');
        for (let i = 0; i < modalButtons.length; i++) {
            if (modalButtons[i].classList.contains('new-game')) {
                modalButtons[i].addEventListener('click', () => {
                    if (this.modalVisible) {
                        this.toggleModalState()
                    }
                    this.showNewGameBox()
                    this.toggleModalState()
                    this.addButtonListener()
                })
            } else if (modalButtons[i].classList.contains('exit-button')) {
                modalButtons[i].addEventListener('click', () => {
                    this.toggleModalState()
                    const newGameButton = document.querySelector('li.new-game')
                    newGameButton.addEventListener('click', () => {
                        this.showNewGameBox()
                        if (!this.modalVisible) {
                            this.toggleModalState()
                        }
                        this.addButtonListener()
                    })
                })
            } else if (modalButtons[i].classList.contains('play-button')) {
                modalButtons[i].addEventListener('click', () => {
                    this.setGameType()
                    this.toggleModalState()
                    this.runNewGame()
                    const newGameButton = document.querySelector('li.new-game')
                    newGameButton.addEventListener('click', () => {
                        this.showNewGameBox()
                        if (!this.modalVisible) {
                            this.toggleModalState()
                        }
                        this.addButtonListener()
                    })
                })
            }
        }        
    }

    /**
     * set game version
     */
    setGameType() {
        const checkBox = document.querySelector('#custom-checkbox-input')
        if (checkBox.checked) {
            objects = objects2
        } else {
            objects = objects1
        }
    }

    /**
     * open dialog window
     */
    toggleModalState() {
        this.modalVisible = !this.modalVisible
        if (this.modalVisible) {
            document.body.classList.add('modal-visible');
        } else {
            document.body.classList.remove('modal-visible');
        }
    }

    /**
     * set dialog window to winner box
     */
    showWinnerBox() {
        let element = document.querySelector('.modal-content')
        element.innerHTML = `<h1>YOU WIN!</h1>
        <div class="modal-menu">
            <button class="modal-button new-game">
                NEW GAME
            </button>
            <button class="modal-button exit-button">
                EXIT
            </button>
        </div>`
    }

    /**
     * set dialog window to looser box
     */
    showLooserBox() {
        let element = document.querySelector('.modal-content')
        element.innerHTML = `<h1>YOU LOSE!</h1>
        <div class="modal-menu">
            <button class="modal-button new-game">
                RETRY
            </button>
            <button class="modal-button exit-button">
                EXIT
            </button>
        </div>`
    }

    /**
     * set dialog window to new game box
     */
    showNewGameBox() {
        let element = document.querySelector('.modal-content')
        element.innerHTML = `<h1>NEW GAME</h1>
        <div class="icon-menu">
            <img src="1verze.png" class="icon">
            <input id="custom-checkbox-input" type="checkbox">
            <label id="custom-checkbox" for="custom-checkbox-input"></label>
            <img src="2verze.png" class="icon">
        </div>
        <div class="icon-menu">
            <button class="modal-button play-button">
                PLAY
            </button>
            <button class="modal-button exit-button">
                EXIT
            </button>
        </div>`
    }

    /**
     * clear dock
     */
    clear() {
        this.parent.remove()
        this.parent = document.createElement('div')
        this.parent.setAttribute('id', 'dock')
        const asideElement = document.querySelector('aside')
        asideElement.appendChild(this.parent)
        this.array = []
        this.index = 0
        this.sum = 0
    }

    /**
     * render gaps and fill them if it saved in localStorage
     */
    render() {
        for (let i = 0; i < this.sumOfPins() - 2; i++) {
            const pin = new Pin(null, null)
            const pinElement = document.createElement('div')
            pinElement.setAttribute('class', 'emptyPin')
            pin.setElement(pinElement)
            this.array.push(pin)
            this.parent.appendChild(pinElement)
        }
        if (localStorage.getItem('dock') != null) {
            for (let i = 0; i < localStorage.getItem('dock'); i++) {
                this.array[i].element.className = 'fullPin'
            }
            this.index = localStorage.getItem('dock') * 1
        } else {
            localStorage.setItem('dock', 0)
        }
    }
}

let myDock = new Dock(document.querySelector('#dock'))
myDock.render()
let myPlan = new Plan(document.querySelector('#plan'), myDock)
myDock.setPlan(myPlan)
myPlan.render()
myPlan.init()